require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get login" do
    get :login
    assert_response :success
  end

  test "should get public" do
    get :public
    assert_response :success
  end

  test "should get account_setting" do
    get :account_setting
    assert_response :success
  end

end
