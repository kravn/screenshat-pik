class CreateScreenshots < ActiveRecord::Migration
  def change
    create_table :screenshots do |t|
      t.references :user
      t.text :description
      t.string :image
      t.boolean :is_featured, default: false
      t.timestamps
    end
  end
end
