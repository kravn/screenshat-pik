class UsersController < ApplicationController

  protect_from_forgery with: :null_session,
                       if: Proc.new { |c| c.request.format =~ %r{application/json} }

  before_action :user_whois, only: [:show, :edit, :update, :destroy]
  before_filter :catch_current_user, except: [:create]

  # GET /users
  # GET /users.json
  def index
    redirect_to root_path
    #@users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    #@user = User.friendly.find(params[:id])
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    redirect_to users_path
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to dashboard_login_path }
        format.json
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if @user.update(user_params)
      flash[:notice] = 'Account Information successfully updated.'
      respond_to do |format|
        format.html { redirect_to dashboard_account_setting_path }
        format.js
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    redirect_to users_path
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:first_name, :last_name, :gender, :email, :username, :password, :salt, :about_me, :slug, :avatar)
  end

  def complete_user_params
    params.require(:user).permit(:first_name, :last_name, :gender, :email, :username, :password, :salt, :about_me, :slug, :avatar)
  end


end
