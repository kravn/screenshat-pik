class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  protect_from_forgery with: :exception

  protected

  def confirm_user_authenticity
    unless session[:id]
      flash[:notice] = "Please log in."
      redirect_to dashboard_login_path
      return false
    else
      return true
    end
  end

  def current_user_session(u)
    session[:id] = u.id
    session[:username] = u.username
  end

  def user_signed_in?
    session[:id].present?
  end

  def catch_current_user
    if user_signed_in?
      @current_user =  User.find(session[:id])
    end

  end

  def user_whois
    @user = User.friendly.find(params[:id])
  end

end
