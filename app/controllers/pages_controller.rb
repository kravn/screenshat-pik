class PagesController < ApplicationController

  before_filter :confirm_user_authenticity, except: :home
  before_filter :catch_current_user

  def home
    @featured_screenshots = Screenshot.where(:is_featured => true).order("created_at DESC").limit(3)
    if user_signed_in?
      redirect_to dashboard_index_path
    end
  end

end
