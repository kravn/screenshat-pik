class DashboardController < ApplicationController

  before_filter :confirm_user_authenticity, except: [:public, :login, :attempt_login]
  before_filter :catch_current_user, except: [:login, :attempt_login]

  def index
  end

  def public
    @user = User.find(params[:id])
  end

  def account_setting
  end

  def login
  end

  def attempt_login
    flash[:notice] = "Invalid username/password combination."
    params.inspect
    authorized_user = User.authenticate(params[:username], params[:password])
    if authorized_user
      current_user_session(authorized_user)
      flash[:notice] = "You are now logged in."
      redirect_to dashboard_index_path
    else
      flash[:notice] = "Invalid username/password. Please try again."
      redirect_to dashboard_login_path
    end
  end

  def logout
    session[:id] = nil
    session[:user] = nil
    flash[:notice] = "You have been logged out."
    redirect_to dashboard_login_path
  end

  private


end
