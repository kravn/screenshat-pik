class ScreenshotsController < ApplicationController

  protect_from_forgery with: :null_session,
                       if: Proc.new { |c| c.request.format =~ %r{application/json} }

  before_filter :confirm_user_authenticity, :catch_current_user
  before_action :set_screenshot, only: [:show, :edit, :update, :destroy]

  # GET /screenshots
  # GET /screenshots.json
  def index
    redirect_to root_path
    #@screenshots = Screenshot.all
  end

  # GET /screenshots/1
  # GET /screenshots/1.json
  def show
    #@user = User.find(session[:id])
    @screenshot = Screenshot.find(params[:id])
    #redirect_to dashboard_index_path
  end

  # GET /screenshots/new
  def new
    @screenshot = Screenshot.new
    @user = User.find(session[:id])
  end

  # GET /screenshots/1/edit
  def edit
    @user = User.find(session[:id])
  end

  # POST /screenshots
  # POST /screenshots.json
  def create
    @screenshot = Screenshot.new(screenshot_params)
    if @screenshot.valid?
      @current_user.screenshots << @screenshot
      flash[:notice] = "Screenshot was successfully created."
      respond_to do |format|
        format.html { redirect_to dashboard_path }
        format.js
      end
    else
      respond_to do |format|
        format.html { render action: 'new' }
        format.js
      end
    end
  end

  # PATCH/PUT /screenshots/1
  # PATCH/PUT /screenshots/1.json
  def update
    respond_to do |format|
      if @screenshot.update(screenshot_params)
        format.html { redirect_to @screenshot, notice: 'Screenshot was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @screenshot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /screenshots/1
  # DELETE /screenshots/1.json
  def destroy
    @screenshot.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_path }
      format.js
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_screenshot
    @screenshot = Screenshot.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def screenshot_params
    params.require(:screenshot).permit(:description, :image, :slug)
  end

end
