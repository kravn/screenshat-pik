class Screenshot < ActiveRecord::Base

  # Relationship and Associations
  belongs_to :user

  has_attached_file :image, :styles => {:medium => "300x300", :thumb => "100x100", :tiny => "50x50", :wall => "600x600" }, :default_url => "//placehold.it/50/ffffff/ddddddg"
  validates_attachment :image, content_type: { content_type: /\Aimage\/.*\Z/ }
  validates_attachment_size :image, less_than: 10.megabytes
  #validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/png']

  validate :check_screenshot_validations

  private

  def check_screenshot_validations
    if !(description.blank? ^ image.blank?) && (description.blank? && image.blank?)
      errors.add(:base, "Description or image can't be null.")
    end
  end

end
