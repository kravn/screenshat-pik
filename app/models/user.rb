require 'digest/sha2'

class User < ActiveRecord::Base

  extend FriendlyId
  friendly_id :username, use: :slugged

  # Relationship and Associations
  has_many :screenshots

  attr_accessor :password

  EMAIL_REGEX = /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :username, presence: true, uniqueness: true
  validates :email, presence: true, format: EMAIL_REGEX, confirmation: true

  validates_length_of :password, :within => 8..25, :on => :create

  before_save :prepare_hashed_password
  after_save :clear_password

  has_attached_file :avatar, :styles => {:medium => "250x250", :thumb => "100x100", :tiny => "50x50", :wall => "600x600" }, :default_url => "//placehold.it/100/ffffff/ddddddg"
  validates_attachment :avatar, content_type: { content_type: /\Aimage\/.*\Z/ }
  validates_attachment_size :avatar, less_than: 10.megabytes

  scope :named, lambda { |first, last| where(:first_name => first, :last_name => last) }
  scope :male_users, where(gender: 'male')
  scope :female_users, where(gender: 'female')

  # MODEL ACTIONS/FUNCTIONS
  def ordered_screenshot_by_creation
    screenshots.order('created_at DESC')
  end

  def fullname
    return "#{first_name} #{last_name}"
  end

  def self.generate_salt(username="")
    Digest::SHA2.hexdigest("User #{username} will be salted with #{Time.now}")
  end

  def self.encrypt_password(password="", salt="")
    Digest::SHA2.hexdigest("Hashed #{password} with #{salt}")
  end

  def password_match?(password="")
    hashed_password == User.encrypt_password(password, salt)
  end

  def self.authenticate(username="", password="")
    user = User.find_by_username(username)
    if user && user.password_match?(password)
      return user
    else
      return false
    end
  end

  private

  def prepare_hashed_password
    unless password.blank?
      self.salt = User.generate_salt(username) if salt.blank?
      self.hashed_password = User.encrypt_password(password, salt)
    end
  end

  def clear_password
    self.password = nil
  end

end
