json.array!(@screenshots) do |screenshot|
  json.extract! screenshot, :id, :references, :description, :image, :is_featured, :default, :false
  json.url screenshot_url(screenshot, format: :json)
end
