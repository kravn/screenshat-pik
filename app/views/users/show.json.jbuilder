json.extract! @user, :id, :first_name, :last_name, :gender, :email, :username, :hashed_password, :salt, :created_at, :updated_at
